package gui;

import monitored.data.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class Gui extends JFrame {
	
	private JPanel panelFin;
	private JButton firstButton, secondButton, thirdButton, fourthButton, fifthButton;
	private JLabel firstLabel, secondLabel, thirdLabel, fourthLabel, fifthLabel;
	private JTextField loadTextField;
	private JTextField firstTextField;
	
	public Gui() {
		
		super("Smart house");
		setLayout(new GridLayout());
		setSize(370,350);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		panelFin = new JPanel();
		
		loadTextField = new JTextField(20);
		
		firstLabel = new JLabel("Count the distinct days that appear in the monitoring data:");
		firstButton = new JButton("Show result");
		firstTextField = new JTextField(3);
		firstTextField.setEditable(false);
		
		secondLabel = new JLabel("For each distinct action, count the number of occurrences:");
		secondButton = new JButton("Show result");
		
		thirdLabel = new JLabel("          Activity count for each day of the log:           ");
		thirdButton = new JButton("Show result");
		
		fourthLabel = new JLabel("For each activity => total duration + filter duration > 10 hours:");
		fourthButton = new JButton("Show result");
		
		fifthLabel = new JLabel("Activities that have 90% duration less  than  5  minutes:");
		fifthButton = new JButton("Show result");
		
		
		firstButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Activities activities = new Activities();
					String fileName = loadTextField.getText();
					activities.activitiesList(fileName);
					firstTextField.setText("" + activities.countDistinctDays());
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		secondButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Activities activities = new Activities();
					String fileName = loadTextField.getText();
					activities.activitiesList(fileName);
					Map<String, Integer> result = activities.countDistinctActivities();
					
					String outputFile = "distinctActivities.txt";
					
					try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile))){
						result.forEach((activity, count) -> {
							try {
								writer.write(activity + "   " + count + " times");
								writer.newLine();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						});
					}
					 catch (IOException e2) {
							e2.printStackTrace();
					}
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		thirdButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Activities activities = new Activities();
					String fileName = loadTextField.getText();
					activities.activitiesList(fileName);
					Map<Integer, Map<String, Long>> result = activities.countActivitiesPerDay();
					
					String outputFile = "distinctActivitiesPerDay.txt";
					
					try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile))){
						result.forEach((day, map) -> {
							map.forEach((activity,count) ->  {
								try {
									writer.write("day" + day + " " + "activity: " + activity + " count " + count);
									writer.newLine();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
							});
								try {
									writer.newLine();
								} catch (IOException e1) {
									e1.printStackTrace();
								}
						});
					}
					 catch (IOException e2) {
							e2.printStackTrace();
					}
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		fourthButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Activities activities = new Activities();
					String fileName = loadTextField.getText();
					activities.activitiesList(fileName);
					Map<String, Long> result = activities.totalDuration();
					
					String outputFile = "activityTotalDuration.txt";
					
					try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile))){
						result.forEach((activity, count) -> {
							try {
								writer.write(activity + " total duration " + count);
								writer.newLine();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						});
					}
					 catch (IOException e2) {
							e2.printStackTrace();
					}
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		fifthButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Activities activities = new Activities();
					String fileName = loadTextField.getText();
					activities.activitiesList(fileName);
					List<String> result = activities.activityFilter();
					
					String outputFile = "activityFilter.txt";
					
					try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(outputFile))){
						result.forEach((activity) -> {
							try {
								writer.write(activity );
								writer.newLine();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						});
					}
					 catch (IOException e2) {
							e2.printStackTrace();
					}
				}
				catch(Exception err){
					JOptionPane.showMessageDialog(null, "Error! Nothing to show!");
				}
			}
		});
		
		panelFin.add(loadTextField);
		panelFin.add(firstLabel);
		panelFin.add(firstButton);
		panelFin.add(firstTextField);
		panelFin.add(secondLabel);
		panelFin.add(secondButton);
		panelFin.add(thirdLabel);
		panelFin.add(thirdButton);
		panelFin.add(fourthLabel);
		panelFin.add(fourthButton);
		panelFin.add(fifthLabel);
		panelFin.add(fifthButton);
		
		add(panelFin);
		setVisible(true);
	}
	
}
