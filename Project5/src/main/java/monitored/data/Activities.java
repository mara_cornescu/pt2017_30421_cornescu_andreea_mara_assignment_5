package monitored.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Activities {

	private List<MonitoredData> monitoredData;
	private Map<String, Integer> distinctActivities;
	private Map<String, Long> activityDurationFilter;
	private Map<Integer, Map<String, Long>> activitiesPerDay;
	private Stream<String> stream;
	private List<String> filterActivities;
	
	
	public List<MonitoredData> activitiesList(String fileName) {
		stream = Stream.empty();
		
		try {
			stream = Files.lines(Paths.get(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		monitoredData = stream.map(MonitoredData::new).collect(Collectors.toList());
		
		return monitoredData;
	}
	
	public long countDistinctDays() {
		return monitoredData.stream().map(data -> data.getStartDay()).distinct().count();
	}
	
	public Map<String, Integer> countDistinctActivities() {
		distinctActivities = monitoredData
				.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, 
						Collectors.summingInt(e -> 1)));
						//Collectors.counting()));
		
		return distinctActivities;
	}
	
	
	public Map<Integer, Map<String, Long>> countActivitiesPerDay() {
		
		activitiesPerDay = monitoredData
				.stream()
				.collect(Collectors
						.groupingBy(MonitoredData::getStartDay, 
									Collectors.groupingBy(MonitoredData::getActivityLabel, 
															Collectors.counting())));
		
		return activitiesPerDay;
	}
	
	
	public Map<String, Long> totalDuration() {
		
		Map<String, Long> dateTimeMap = monitoredData.stream()
										.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, 
												Collectors.summingLong(MonitoredData::getDateTime)));

			
		activityDurationFilter = dateTimeMap.keySet().stream().filter(k -> dateTimeMap.get(k) > 10)
										.collect(Collectors.toMap(k -> k, k -> dateTimeMap.get(k)));
		
		return activityDurationFilter;
	}
	
	public List<String> activityFilter() {
		
		Map<String, Integer> numberOfActivities;
		Map<String, Integer> minutesPerActivity;
				
		numberOfActivities = monitoredData
					.stream()
					.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, 
									Collectors.summingInt(e -> 1)));
		
		minutesPerActivity = monitoredData
						.stream()
						.filter(p -> (p.getEndHour() == p.getStartHour()) && ((p.getEndMinute() - p.getStartMinute()) < 5)) 
						.collect(Collectors.groupingBy(MonitoredData::getActivityLabel, 
								Collectors.summingInt(e -> 1)));

		
		filterActivities = minutesPerActivity.keySet().stream()
				.filter(k -> numberOfActivities.get(k).longValue() * 0.9 < minutesPerActivity.get(k))
				.collect(Collectors.toList());
	
		return filterActivities;
	}
}
