package monitored.data;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MonitoredData {
	
	private Date startTime;
	private Date endTime;
	private String activityLabel;
	
	
	public MonitoredData(String startTime, String endTime, String activityLabel) {
		setStartTime(startTime);
		setEndTime(endTime);
		setActivityLabel(activityLabel);
	}
	
	public MonitoredData(String monitoredData) {
		String[] data = monitoredData.split("		");
		setStartTime(data[0]);
		setEndTime(data[1]);
		setActivityLabel(data[2].trim());
	}
	
	public Date getStartTime() {
		return startTime;
	}
	
	public int getStartDay() {
		 Calendar cal = Calendar.getInstance();
		 cal.setTime(startTime);
		 return cal.get(Calendar.DAY_OF_MONTH);
	}
	
	public int getStartHour() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startTime);
		
		return cal.get(Calendar.HOUR);
	}
	
	public int getStartMinute() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(startTime);
		
		return cal.get(Calendar.MINUTE);
	}
	
	
	public void setStartTime(String startTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
		try {
			this.startTime = sdf.parse(startTime); 
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public Date getEndTime() {
		return endTime;
	}
	
	public int getEndHour() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(endTime);
		
		return cal.get(Calendar.HOUR);
	}
	
	public int getEndMinute() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(endTime);
		
		return cal.get(Calendar.MINUTE);
	}
	
	public void setEndTime(String endTime) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh:mm:ss");
		try {
			this.endTime = sdf.parse(endTime); 
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	public String getActivityLabel() {
		return activityLabel;
	}
	
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	public long getDateTime() {
		long duration;

		if(this.getEndHour() >= getStartHour()) {
			duration = this.getEndHour() - this.getStartHour();
		}
		else {
			duration = 12 - this.getStartHour() + this.getEndHour();
		}
		
		return duration;
	}
}
